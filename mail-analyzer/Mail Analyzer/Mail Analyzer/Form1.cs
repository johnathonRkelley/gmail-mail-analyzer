﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ActiveUp.Net.Mail;
using MailKit;
using MailKit.Net.Imap;
using System.Threading;
using MailKit.Search;

namespace Mail_Analyzer
{
    public partial class Form1 : Form
    {
        //https://stackoverflow.com/questions/7056715/reading-emails-from-gmail-in-c-sharp
        public UserAccount account;

        public Form1()
        {
            InitializeComponent();
            account = new UserAccount();

            grabbingDataFromIMAP();

            List<EmailGroup> emails = new FileReader().parseDataFromTextFile();
            buildInformationPanelWithTableLayout(emails);
        }

        public void buildInformationPanelWithTableLayout(List<EmailGroup> emails)
        {
            TableLayoutPanel table = new TableLayoutPanel();
            table.Dock = DockStyle.Fill;
            table.ColumnCount = 1;
            table.RowCount = 2;
            table.ColumnStyles.Add(columnStyleforTable());

            int totalAmountOfEmails = countOfTotalEmails(emails);

            table.RowCount = emails.Count;
            foreach (EmailGroup email in emails)
            {
                email.setPercent(totalAmountOfEmails);
            }

            int row = 0;

            foreach (EmailGroup email in emails)
            {
                table.Controls.Add(new frmPercent(email, totalAmountOfEmails, this) { TopLevel = false, Visible = true }, 0, row);
                row++;
            }
            table.AutoScroll = true;
            panMain.Controls.Add(table);
        }

        public int countOfTotalEmails(List<EmailGroup> emails)
        {
            int count = 0;
            foreach (EmailGroup email in emails)
            {
                count += email.getListCount();
            }
            return count;
        }

        public RowStyle rowStyleForTable()
        {
            return new RowStyle(SizeType.Absolute, 60f);
        }

        public ColumnStyle columnStyleforTable(){
            return new ColumnStyle(SizeType.AutoSize);
        }

        public void grabbingDataFromIMAP()
        {
            List<Email> listOfEmails = new List<Email>();
            List<String> emails = new List<String>();

            using (var client = new ImapClient())
            {
                client.Connect("imap.gmail.com", 993, true);
                client.Authenticate(account.getUserEmailAddress(), account.getUserPassword());

                var inbox = client.Inbox;
                inbox.Open(FolderAccess.ReadWrite);

                int index = 0;

                DateTime startTime = DateTime.Now; //Used for Time Testing
                foreach (var summary in inbox.Fetch(0, -1, MessageSummaryItems.Envelope))
                {
                    String fromTag = summary.Envelope.From.ToString();
                    int indexOf = fromTag.IndexOf('<');
                    Console.WriteLine("from: {0}", fromTag);

                     emails.Add(new Email(summary.Envelope.From.ToString(), index).getOutputStringForFile());
                    index++;
                }
                DateTime endTime = DateTime.Now;
                Double elapsedMillisecs = ((TimeSpan)(endTime - startTime)).TotalMilliseconds;
                Console.WriteLine("Final Amount of Seconds : {0}", elapsedMillisecs);
                

                client.Disconnect(true);
            }
            
            new FileReader().writeToTextFile(emails);
        }

        public void deleteEmailGroups(EmailGroup emails)
        {
            List<int> indexes = emails.emailIndexes;
            
                using (var client = new ImapClient())
                {
                    client.Connect("imap.gmail.com", 993, true);
                    client.Authenticate(account.getUserEmailAddress(), account.getUserPassword());

                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadWrite);

                    foreach (int index in indexes)
                    {
                        inbox.AddFlags(index, MessageFlags.Deleted, true);
                        inbox.Expunge();

                    }
                    client.Disconnect(true);
                }
        }
    }
}
