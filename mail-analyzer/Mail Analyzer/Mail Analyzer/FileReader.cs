﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mail_Analyzer
{
    public class FileReader
    {
        public FileReader()
        {

        }

        public void writeToTextFile(List<String> emails)
        {
            System.IO.File.WriteAllLines("test.txt", emails);
        }

        public List<EmailGroup> parseDataFromTextFile()
        {
            string[] textFileData = System.IO.File.ReadAllLines("test.txt");

            List<EmailGroup> emails = new List<EmailGroup>();

            foreach (String line in textFileData)
            {
                bool exists = false;

                int index = line.IndexOf("from");
                int id = Int32.Parse(line.Substring(3, index - 3));
                string from = line.Substring(index + 5);

                foreach (EmailGroup group in emails)
                {
                    String emailsFrom = group.from;

                    if (emailsFrom.Equals(from))
                    {
                        group.addToList(id);
                        exists = true;
                        break;
                    }
                }

                if (exists == false)
                {
                    EmailGroup group = new EmailGroup(from);
                    group.addToList(id);
                    emails.Add(group);
                }
            }

            List<EmailGroup> sortedEmails = emails.OrderByDescending(o => o.getListCount()).ToList<EmailGroup>();

            return sortedEmails;
        }
    }
}
