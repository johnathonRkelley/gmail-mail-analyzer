﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mail_Analyzer
{
    
    public class UserAccount
    {
        private String EMAIL_ACCOUNT_ADDRESS;
        private String EMAIL_PASSWORD;

        public UserAccount()
        {
            getUserData();
        }

        public void getUserData()
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Johnathon\Desktop\userAccount.txt");
            EMAIL_ACCOUNT_ADDRESS = lines[0];
            EMAIL_PASSWORD = lines[1];
        }

        public string getUserEmailAddress(){
            return EMAIL_ACCOUNT_ADDRESS;
        }

        public string getUserPassword(){
            return EMAIL_PASSWORD;
        }
    }
}
