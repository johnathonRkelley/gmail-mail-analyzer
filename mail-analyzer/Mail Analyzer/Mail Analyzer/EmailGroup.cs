﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mail_Analyzer
{
    public class EmailGroup
    {
        public List<int> emailIndexes;
        public String from;
        public double percent; 
        
        public EmailGroup(String from)
        {
            this.from = from;
            percent = 0.0;
            emailIndexes = new List<int>();
        }
        public EmailGroup(String from, int count)
        {
            this.from = from;
            percent = 0.0;
            emailIndexes = new List<int>();
        }

        public void addToList(int index)
        {
            emailIndexes.Add(index);
        }

        public int getListCount()
        {
            return emailIndexes.Count;
        }

        public void setPercent(int totalAmountOfEmails)
        {
            percent =  ((double)emailIndexes.Count / (double)totalAmountOfEmails) * 100;
        }
    }
}
