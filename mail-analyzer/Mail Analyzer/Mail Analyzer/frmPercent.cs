﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mail_Analyzer
{
    public partial class frmPercent : Form
    {
        public EmailGroup emailGroup;
        public Form1 form;
        public frmPercent(EmailGroup emails, int totalEmails, Form1 form)
        {
            InitializeComponent();
            emailGroup = emails;
            this.form = form;
            fixPanel(emails, totalEmails);
        }

        public void fixPanel(EmailGroup emails, int totalEmails)
        {
            lblName.Text = emails.from;

            buildRightLabel(emails.getListCount(), totalEmails);
        }

        public void buildRightLabel(int countOfEmails, int totalEmails)
        {
            String labelName = "";

            double percent = ((double)countOfEmails / (double)totalEmails) * 100;
            int percentToWholeNumber = (int)percent;
            progressBar1.Value = percentToWholeNumber;
            percent.ToString("#.##", CultureInfo.InvariantCulture);
            labelName = percent.ToString("#.##", CultureInfo.InvariantCulture) + "% (" + countOfEmails + ")";

            lblNumber.Text = labelName;
        }

        private void lblName_Click(object sender, EventArgs e)
        {
            String toBeCopied = "from:" + lblName.Text;
            Clipboard.SetText(toBeCopied);
            lblName.BackColor = Color.Green;
            deleteEmailGroup();
        }

        public void deleteEmailGroup()
        {
            form.deleteEmailGroups(emailGroup);
            lblName.Text = lblName.Text + " was deleted";
        }
    }
}
