﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mail_Analyzer
{

    public class Email
    {
        public String EMAIL_FROM;
        public int count;
        public double percent;
        public int index;
        public Email()
        {

        }

        public Email(String from, int index)
        {
            EMAIL_FROM = from;
            count = 1;
            percent = 0.0;
            this.index = index;
        }

        public void increaseCount()
        {
            count++;
        }

        public void getEmailAndCount()
        {
            Console.WriteLine("From : {0} | Count : {1}", EMAIL_FROM, count);
        }

        public void setupEmail(String from)
        {
            EMAIL_FROM = from;
            count = 1;
        }
        public void setPercent(int totalAmountOfEmails)
        {
            percent = ((double)count / (double)totalAmountOfEmails) * 100;
        }

        public String getOutputStringForFile()
        {
            return "id=" + index.ToString() + "from=" + EMAIL_FROM;
        }
    }
}
